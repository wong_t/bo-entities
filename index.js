'use strict';

const UserEntity = require('./entities/UserEntity');
const UserProfileEntity = require('./entities/UserProfileEntity');

module.exports = {
  UserEntity: UserEntity,
  UserProfileEntity: UserProfileEntity
};