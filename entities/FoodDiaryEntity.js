'use strict';

let log;


module.exports = class FoodDiaryEntity {

  // foodDiary
  // (reference User) userId ex : users/YA99XFQx69Yh6SmaZLDytoXcpFw2
  // (String) type ex: diner
  // (Number | integer) duration ex: 25
  // (Number | integer) numberDish ex : 3
  // (String) description ex : ‘lorem ipsum ...’
  // (Date | ISO format date time) createdAt, ex : ‘2020-04-08 21:00:00’
  // (Date | ISO format date time) updateAt, ex : ‘2020-04-08 21:00:00’
  
  // userId
  // type
  // duration
  // numberDish
  // description
  // createdAt
  // updateAt

  /**
  * Creates a new FoodDiaryEntity instance
  * userId, type, duration, numberDish, description, createdAt, updateAt, options
  * 
  * args[0] userId
  * args[1] type
  * args[2] duration
  * args[3] numberDish
  * args[4] description
  * args[5] createdAt
  * args[6] updateAt
  */
  constructor(args) {
    // 
    if (Array.isArray(args)) {
      this.userId = args[0] || null;
      this.type = args[1] || null;
      this.duration = args[2] || null;
      this.numberDish = args[3] || null;
      this.description = args[4] || null;
      this.createdAt = args[5] || null;
      this.updateAt = args[6] || null;
      this.options = args[7] || null;
      return;
    }
    // 
    this.userId = args['userId'] || null;
    this.type = args['type'] || null;
    this.duration = args['duration'] || null;
    this.numberDish = args['numberDish'] || null;
    this.description = args['description'] || null;
    this.createdAt = args['createdAt'] || null;
    this.updateAt = args['updateAt'] || null;
    this.options = args['options'] || null;
    // 
    if (
      this.options
      && this.options.vue
      && this.options.vue.$Log
    ) {
      log = new this.options.vue.$Log('FoodDiaryEntity')
      log.info('FoodDiaryEntity', this.toJSON())
    }
  }
  /**
   * 
   * @param user 
   * @param callback 
   */
  toJSON () {
    try {
      return JSON.stringify({
        userId: this.userId,
        type: this.type,
        duration: this.duration,
        numberDish: this.numberDish,
        description: this.description,
        createdAt: this.createdAt,
        updateAt: this.updateAt,
        options: this.options,
      });
    } catch (err) {
      log.error(err);
      throw err
    }
  }
};