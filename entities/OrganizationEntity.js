'use strict';

let log;


module.exports = class OrganizationEntity {

  // organizations (partenaire, association,...)
  // (String) title
  // (String) slug
  // (String) phone
  // (String) name
  // (String) email
  // (String) description

  // title
  // slug
  // phone
  // name
  // email
  // description
  // options

  /**
  * Creates a new OrganizationEntity instance
  * title, slug, phone, name, email, description, options
  * 
  * args[0] title
  * args[1] slug
  * args[2] phone
  * args[3] name
  * args[4] email
  * args[5] description
  * args[6] options
  */
  constructor(args) {
    // 
    if (Array.isArray(args)) {
      this.title = args[0] || null;
      this.slug = args[1] || null;
      this.phone = args[2] || null;
      this.name = args[3] || null;
      this.email = args[4] || null;
      this.description = args[5] || null;
      this.options = args[6] || null;
      return;
    }
    // 
    this.title = args['title'] || null;
    this.slug = args['slug'] || null;
    this.phone = args['phone'] || null;
    this.name = args['name'] || null;
    this.email = args['email'] || null;
    this.description = args['description'] || null;
    this.options = args['options'] || null;
    // 
    if (
      this.options
      && this.options.vue
      && this.options.vue.$Log
    ) {
      log = new this.options.vue.$Log('OrganizationEntity')
      log.info('OrganizationEntity', this.toJSON())
    }
  }
  /**
   * 
   * @param user 
   * @param callback 
   */
  toJSON () {
    try {
      return JSON.stringify({
        title: this.title,
        slug: this.slug,
        phone: this.phone,
        name: this.name,
        email: this.email,
        description: this.description,
        options: this.options,
      });
    } catch (err) {
      log.error(err);
      throw err
    }
  }
};