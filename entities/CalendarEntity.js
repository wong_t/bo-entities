'use strict';

let log;


module.exports = class CalendarEntity {

  // calendar
  // (reference User) userId ex : users/YA99XFQx69Yh6SmaZLDytoXcpFw2
  // (String) title
  // (String) description
  // (String | ISO format date time) dateDebut, ex : ‘2020-04-08 21:00:00’
  // (String | ISO format date time) dateFin, ex : ‘2020-04-08 21:00:00’
  // (String | ISO format date time) createdAt, ex : ‘2020-04-08 21:00:00’
  // (String | ISO format date time) updateAt, ex : ‘2020-04-08 21:00:00’
  
  // userId
  // title
  // dateDebut
  // dateFin
  // createdAt
  // updateAt

  /**
  * Creates a new CalendarEntity instance
  * userId, address, city, country, phone, zip, createdAt, updateAt, options
  * 
  * args[0] userId
  * args[1] title
  * args[2] dateDebut
  * args[3] dateFin
  * args[4] createdAt
  * args[5] updateAt
  * args[6] options
  */
  constructor(args) {
    // 
    if (Array.isArray(args)) {
      this.userId = args[0] || null;
      this.title = args[1] || null;
      this.dateDebut = args[2] || null;
      this.dateFin = args[3] || null;
      this.createdAt = args[4] || null;
      this.updateAt = args[5] || null;
      this.options = args[6] || null;
      return;
    }
    // 
    this.userId = args['userId'] || null;
    this.title = args['title'] || null;
    this.dateDebut = args['dateDebut'] || null;
    this.dateFin = args['dateFin'] || null;
    this.createdAt = args['createdAt'] || null;
    this.updateAt = args['updateAt'] || null;
    this.options = args['options'] || null;
    // 
    if (
      this.options
      && this.options.vue
      && this.options.vue.$Log
    ) {
      log = new this.options.vue.$Log('CalendarEntity')
      log.info('CalendarEntity', this.toJSON())
    }
  }
  /**
   * 
   * @param user 
   * @param callback 
   */
  toJSON () {
    try {
      return JSON.stringify({
        userId: this.userId,
        title: this.title,
        dateDebut: this.dateDebut,
        dateFin: this.dateFin,
        createdAt: this.createdAt,
        updateAt: this.updateAt,
        options: this.options,
      });
    } catch (err) {
      log.error(err);
      throw err
    }
  }
};