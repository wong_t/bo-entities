'use strict';

let log;


module.exports = class MoodEntity {

  // mood
  // (reference User) userId ex : users/YA99XFQx69Yh6SmaZLDytoXcpFw2
  // (String | ISO format date time) createdAt, ex : ‘2020-04-08 21:00:00’
  // (String) mood : ex : existe t-il des standards ? HAPPY | SAD | etc.
  
  // userId
  // createdAt
  // mood
  // options

  /**
  * Creates a new MoodEntity instance
  * userId, createdAt, mood, options
  * 
  * args[0] userId
  * args[1] createdAt
  * args[2] mood
  * args[3] options
  */
  constructor(args) {
    // 
    if (Array.isArray(args)) {
      this.userId = args[0] || null;
      this.createdAt = args[1] || null;
      this.mood = args[2] || null;
      this.options = args[3] || null;
      return;
    }
    // 
    this.userId = args['userId'] || null;
    this.createdAt = args['createdAt'] || null;
    this.mood = args['mood'] || null;
    this.options = args['options'] || null;
    // 
    if (
      this.options
      && this.options.vue
      && this.options.vue.$Log
    ) {
      log = new this.options.vue.$Log('MoodEntity')
      log.info('MoodEntity', this.toJSON())
    }
  }
  /**
   * 
   * @param user 
   * @param callback 
   */
  toJSON () {
    try {
      return JSON.stringify({
        userId: this.userId,
        createdAt: this.createdAt,
        mood: this.mood,
        options: this.options,
      });
    } catch (err) {
      log.error(err);
      throw err
    }
  }
};