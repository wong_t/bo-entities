'use strict';

let log;


module.exports = class EventEntity {

  // events
  // (reference User) userId ex : users/YA99XFQx69Yh6SmaZLDytoXcpFw2
  // (String | ISO format date time) createdAt, ex : ‘2020-04-08 21:00:00’
  // (String | ISO format date time) updateAt, ex : ‘2020-04-08 21:00:00’
  // (String) title
  // (String) description
  // (String | ISO format date time) eventDate, ex : ‘2020-04-08 21:00:00’
  // (Array de references Users) guests (invités) 
  // ex : array JSON de uid auth user, exemple
  // [
  //   "users/YA99XFQx69Yh6SmaZLDytoXcpFw2",
  //   "users/dqe5L43ndfORaOPX6PEePVTSR1u2",
  //   "users/dqe5L43ndfORaOPX6PEePVTSR1u2"
  // ]

  // userId
  // createdAt
  // updateAt
  // title
  // description
  // eventDate
  // guests
  // user
  // options

  /**
  * Creates a new EventEntity instance
  * userId, createdAt, updateAt, title, description, eventDate, guests, user, options
  * 
  * args[0] userId
  * args[1] createdAt
  * args[2] updateAt
  * args[3] title
  * args[4] description
  * args[5] eventDate
  * args[6] guests
  * args[7] user
  * args[8] options
  */
  constructor(args) {
    // 
    if (Array.isArray(args)) {
      this.userId = args[0] || null;
      this.createdAt = args[1] || null;
      this.updateAt = args[2] || null;
      this.title = args[3] || null;
      this.description = args[4] || null;
      this.eventDate = args[5] || null;
      this.guests = args[6] || null;
      this.user = args[7] || null;
      this.options = args[8] || null;
      return;
    }
    // 
    this.userId = args['userId'] || null;
    this.createdAt = args['createdAt'] || null;
    this.updateAt = args['updateAt'] || null;
    this.title = args['title'] || null;
    this.description = args['description'] || null;
    this.eventDate = args['eventDate'] || null;
    this.guests = args['guests'] || null;
    this.user = args['user'] || null;
    this.options = args['options'] || null;
    // 
    if (
      this.options
      && this.options.vue
      && this.options.vue.$Log
    ) {
      log = new this.options.vue.$Log('EventEntity')
      log.info('EventEntity', this.toJSON())
    }
  }
  /**
   * 
   * @param user 
   * @param callback 
   */
  toJSON () {
    try {
      return JSON.stringify({
        userId: this.userId,
        createdAt: this.createdAt,
        updateAt: this.updateAt,
        title: this.title,
        description: this.description,
        eventDate: this.eventDate,
        guests: this.guests,
        user: this.user,
        options: this.options,
      });
    } catch (err) {
      log.error(err);
      throw err
    }
  }
};