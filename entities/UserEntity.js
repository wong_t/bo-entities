'use strict';

let log;


module.exports = class UserEntity {
  
  // uid
  // email
  // emailVerified
  // phoneNumber
  // password
  // displayName
  // photoURL
  // disabled
  // profile
  // options

  /**
  * Creates a new UserEntity instance
  * uid, email, emailVerified, phoneNumber, password, displayName, photoURL, disabled, profile, options
  * 
  * args[0] uid
  * args[1] email
  * args[2] emailVerified
  * args[3] phoneNumber
  * args[4] password
  * args[5] displayName
  * args[6] photoURL
  * args[7] disabled
  * args[8] profile
  * args[9] options
  */
  constructor(args) {
    // 
    if (Array.isArray(args)) {
      this.uid = args[0] || null;
      this.email = args[1] || null;
      this.emailVerified = args[2] || false;
      this.phoneNumber = args[3] || null;
      this.password = args[4] || null;
      this.displayName = args[5] || null;
      this.photoURL = args[6] || null;
      this.disabled = args[7] || false;
      this.profile = args[8] || false;
      this.options = args[9] || null;
      return;
    }
    // 
    this.uid = args['uid'] || null;
    this.email = args['email'] || null;
    this.emailVerified = args['emailVerified'] || false;
    this.phoneNumber = args['phoneNumber'] || null;
    this.password = args['password'] || null;
    this.displayName = args['displayName'] || null;
    this.photoURL = args['photoURL'] || null;
    this.disabled = args['disabled'] || false;
    this.profile = args['profile'] || null;
    this.options = args['options'] || null;
    // 
    if (
      this.options
      && this.options.vue
      && this.options.vue.$Log
    ) {
      log = new this.options.vue.$Log('UserEntity')
      log.info('UserEntity', this.toJSON())
    }
  }
  /**
   * 
   * @param user 
   * @param callback 
   */
  toJSON () {
    try {
      return JSON.stringify({
        uid: this.uid,
        email: this.email,
        emailVerified: this.emailVerified,
        phoneNumber: this.phoneNumber,
        password: this.password,
        displayName: this.displayName,
        photoURL: this.photoURL,
        disabled: this.disabled
      });
    } catch (err) {
      log.error(err);
      throw err
    }
  }
};