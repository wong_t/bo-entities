'use strict';

let log;


module.exports = class ArticleEntity {

  // articles
  // (String) title
  // (String) category
  // (String) description
  // (String) imageUrl
  // (String) slug
  // (String) text

  
  // title
  // category
  // description
  // imageUrl
  // slug
  // text
  // options

  /**
  * Creates a new ArticleEntity instance
  * title, category, description, imageUrl, slug, text, options
  * 
  * args[0] title
  * args[1] category
  * args[2] description
  * args[3] imageUrl
  * args[4] slug
  * args[5] text
  * args[6] options
  */
  constructor(args) {
    // 
    if (Array.isArray(args)) {
      this.title = args[0] || null;
      this.category = args[1] || null;
      this.description = args[2] || null;
      this.imageUrl = args[3] || null;
      this.slug = args[4] || null;
      this.text = args[5] || null;
      this.options = args[6] || null;
      return;
    }
    // 
    this.title = args['title'] || null;
    this.category = args['category'] || null;
    this.description = args['description'] || null;
    this.imageUrl = args['imageUrl'] || null;
    this.slug = args['slug'] || null;
    this.text = args['text'] || null;
    this.options = args['options'] || null;
    // 
    if (
      this.options
      && this.options.vue
      && this.options.vue.$Log
    ) {
      log = new this.options.vue.$Log('ArticleEntity')
      log.info('ArticleEntity', this.toJSON())
    }
  }
  /**
   * 
   * @param user 
   * @param callback 
   */
  toJSON () {
    try {
      return JSON.stringify({
        title: this.title,
        category: this.category,
        description: this.description,
        imageUrl: this.imageUrl,
        slug: this.slug,
        text: this.text,
        options: this.options,
      });
    } catch (err) {
      log.error(err);
      throw err
    }
  }
};