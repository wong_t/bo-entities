'use strict';

let log;


module.exports = class AddressEntity {
  
  // userId
  // address
  // city
  // country
  // phone
  // zip
  // createdAt
  // updateAt

  /**
  * Creates a new AddressEntity instance
  * userId, address, city, country, phone, zip, createdAt, updateAt, options
  * 
  * args[0] userId
  * args[1] address
  * args[2] city
  * args[3] country
  * args[4] phone
  * args[5] zip
  * args[6] createdAt
  * args[7] updateAt
  */
  constructor(args) {
    // 
    if (Array.isArray(args)) {
      this.userId = args[0] || null;
      this.address = args[1] || null;
      this.city = args[2] || null;
      this.country = args[3] || null;
      this.phone = args[4] || null;
      this.zip = args[5] || null;
      this.createdAt = args[6] || null;
      this.updateAt = args[7] || null;
      this.options = args[8] || null;
      return;
    }
    // 
    this.userId = args['userId'] || null;
    this.address = args['address'] || null;
    this.city = args['city'] || null;
    this.country = args['country'] || null;
    this.phone = args['phone'] || null;
    this.zip = args['zip'] || null;
    this.createdAt = args['createdAt'] || null;
    this.updateAt = args['updateAt'] || null;
    this.options = args['options'] || null;
    // 
    if (
      this.options
      && this.options.vue
      && this.options.vue.$Log
    ) {
      log = new this.options.vue.$Log('AddressEntity')
      log.info('AddressEntity', this.toJSON())
    }
  }
  /**
   * 
   * @param user 
   * @param callback 
   */
  toJSON () {
    try {
      return JSON.stringify({
        userId: this.userId,
        address: this.address,
        city: this.city,
        country: this.country,
        phone: this.phone,
        zip: this.zip,
        createdAt: this.createdAt,
        updateAt: this.updateAt,
        options: this.options
      });
    } catch (err) {
      log.error(err);
      throw err
    }
  }
};