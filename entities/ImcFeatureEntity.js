'use strict';

let log;


module.exports = class ImcFeatureEntity {
  

  // imcFeatures
  // (reference User) userId ex : users/YA99XFQx69Yh6SmaZLDytoXcpFw2
  // (Number | integer) weight ex : 62
  // (Number | integer) height ex : 62
  // (Number | float) resultIMC ex : 21.2
  // (String | ISO format date time) createdAt, ex : ‘2020-04-08 21:00:00’
  // (String | ISO format date time) updateAt, ex : ‘2020-04-08 21:00:00’
  // (String | ISO format date) recordDate, ex : ‘2020-04-08’

  // userId
  // weight
  // height
  // resultIMC
  // createdAt
  // updateAt
  // recordDate
  // options

  /**
  * Creates a new ImcFeatureEntity instance
  * userId, weight, height, resultIMC, createdAt, updateAt, recordDate, options
  * 
  * args[0] this.userId
  * args[1] this.weight
  * args[2] this.height
  * args[3] this.resultIMC
  * args[4] this.createdAt
  * args[5] this.updateAt
  * args[6] this.recordDate
  * args[7] this.options
  */
  constructor(args) {
    // 
    if (Array.isArray(args)) {
      this.userId = args[0] || null;
      this.weight = args[1] || null;
      this.height = args[2] || null;
      this.resultIMC = args[3] || null;
      this.createdAt = args[4] || null;
      this.updateAt = args[5] || null;
      this.recordDate = args[6] || null;
      this.options = args[7] || null;
      return;
    }
    // 
    this.userId = args['userId'] || null;
    this.weight = args['weight'] || null;
    this.height = args['height'] || null;
    this.resultIMC = args['resultIMC'] || null;
    this.createdAt = args['createdAt'] || null;
    this.updateAt = args['updateAt'] || null;
    this.recordDate = args['recordDate'] || null;
    this.options = args['options'] || null;
    // 
    if (
      this.options
      && this.options.vue
      && this.options.vue.$Log
    ) {
      log = new this.options.vue.$Log('ImcFeatureEntity')
      log.info('ImcFeatureEntity', this.toJSON())
    }
  }
  /**
   * 
   * @param user 
   * @param callback 
   */
  toJSON () {
    try {
      return JSON.stringify({
        userId: this.userId,
        weight: this.weight,
        height: this.height,
        resultIMC: this.resultIMC,
        createdAt: this.createdAt,
        updateAt: this.updateAt,
        recordDate: this.recordDate,
        options: this.options,
      });
    } catch (err) {
      log.error(err);
      throw err
    }
  }
};