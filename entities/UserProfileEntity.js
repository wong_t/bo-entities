'use strict';

let log;


module.exports = class UserProfileEntity {

  /*
    userId
    fName
    lName
    email
    sex
    birthDate
    newsletter
    photo
    status
    role
    createdAt
    updatedAt
  */
  


  /**
  * Creates a new UserProfileEntity instance
  * userId, fName, lName, email, sex, birthDate, newsletter, photo, status, role, createdAt, updatedAt, options
  * 
  * args[0] userId
  * args[1] fName
  * args[2] lName
  * args[3] email
  * args[4] sex
  * args[5] birthDate
  * args[6] newsletter
  * args[7] photo
  * args[8] status
  * args[9] role
  * args[10] createdAt
  * args[11] updatedAt
  * args[12] options
  */
  constructor(args) {
    // 
    if (Array.isArray(args)) {
      this.userId = args[0] || null;
      this.fName = args[1] || null;
      this.lName = args[2] || null;
      this.email = args[3] || null;
      this.sex = args[4] || null;
      this.birthDate = args[5] || null;
      this.newsletter = args[6] || null;
      this.photo = args[7] || null;
      this.status = args[8] || null;
      this.role = args[9] || null;
      this.createdAt = args[10] || null;
      this.updatedAt = args[11] || null;
      this.options = args[12] || null;
      return;
    }
    // 
    this.userId = args['userId'] || null;
    this.fName = args['fName'] || null;
    this.lName = args['lName'] || null;
    this.email = args['email'] || null;
    this.sex = args['sex'] || null;
    this.birthDate = args['birthDate'] || null;
    this.newsletter = args['newsletter'] || null;
    this.photo = args['photo'] || null;
    this.status = args['status'] || null;
    this.role = args['role'] || null;
    this.createdAt = args['createdAt'] || null;
    this.updatedAt = args['updatedAt'] || null;
    this.options = args['options'] || null;
    // 
    if (
      this.options
      && this.options.vue
      && this.options.vue.$Log
    ) {
      log = new this.options.vue.$Log('UserProfileEntity')
      log.info('UserProfileEntity', this.toJSON())
    }
  }
  /**
   * 
   * @param user 
   * @param callback 
   */
  toJSON () {
    try {
      return JSON.stringify({
        userId: this.userId,
        fName: this.fName,
        lName: this.lName,
        email: this.email,
        sex: this.sex,
        birthDate: this.birthDate,
        newsletter: this.newsletter,
        photo: this.photo,
        status: this.status,
        role: this.role,
        createdAt: this.createdAt,
        updatedAt: this.updatedAt
      });
    } catch (err) {
      log.error(err);
      throw err
    }
  }
};